﻿using System;
using SciterSharp.Interop;

namespace TesteAPNG
{
	class Program
	{
		[STAThread]
		public static void Main(string[] args)
		{
			LWindow window = new LWindow();
			window.LoadPage();
			
			PInvokeUtils.RunMsgLoop();
		}
	}
}