﻿using System;
using SciterSharp;

namespace TesteAPNG
{
	public class LWindow
	{
		string baseurl;
		SciterWindow window;
		
		public LWindow()
		{
			this.baseurl = AppDomain.CurrentDomain.BaseDirectory;
		
			#if (DEBUG)
				this.baseurl = this.baseurl.Replace(@"\bin\Debug", "");
			#endif
		
			this.window = new SciterWindow();
			this.window.CreateMainWindow(800, 450);
			this.window.CenterTopLevelWindow();
		}
		
		public void LoadPage()
		{
			string filepath = String.Format("file://{0}/res/ui.html", this.baseurl);
			this.window.LoadPage(filepath);
			Console.WriteLine(filepath);
			this.window.Show();
		}
	}
}

